﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Passenger.Infrastructure.Commands;
using Microsoft.AspNetCore.Mvc;
using Passenger.Infrastructure.Commands.Drivers;
using Microsoft.AspNetCore.Authorization;

namespace Passenger.Api.Controllers
{
    [Route("drivers/routes")]
    public class DriverRoutesController : ApiControllerBase
    {
        protected DriverRoutesController(ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {

        }

        //[Authorize]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateDriverRoute command)
        {
            await DispatchAsync(command);

            return NoContent();
        }

        //[Authorize]
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            var command = new DeleteDriverRoute()
            {
                Name = name
            };

            await DispatchAsync(command);
            return NoContent();
        }
    }
}