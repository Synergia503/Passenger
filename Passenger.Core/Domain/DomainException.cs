﻿using System;

namespace Passenger.Core.Domain
{
    public class DomainException : PassengerException
    {
        protected DomainException()
        {
        }

        protected DomainException(string code) : base(code)
        {

        }

        public DomainException(string message, params object[] args) : base(string.Empty, message, args)
        {

        }

        public DomainException(string code, string message, params object[] args) : base(null, code, message, args)
        {

        }

        public DomainException(Exception innerException, string message, params object[] args) : base(innerException, string.Empty, message, args)
        {

        }

        public DomainException(Exception innerException, string code, string message, params object[] args) : base(code, String.Format(message, args), innerException)
        {

        }
    }
}