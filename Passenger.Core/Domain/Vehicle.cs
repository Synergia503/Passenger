﻿using System;

namespace Passenger.Core.Domain
{
    public class Vehicle
    {
        protected Vehicle()
        {

        }

        protected Vehicle(string make, string name, int seats)
        {
            SetMake(make);
            SetName(name);
            SetSeats(seats);
        }

        public string Name { get; protected set; }
        public int Seats { get; protected set; }
        public string Make { get; protected set; }

        private void SetMake(string make)
        {
            if (string.IsNullOrWhiteSpace(make))
            {
                throw new Exception("Make can not be empty.");
            }

            Make = make;
        }

        private void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Name can not be empty.");
            }

            Name = name;
        }

        private void SetSeats(int seats)
        {

            if (seats < 0)
            {
                throw new Exception("Seats must be greater than 0.");
            }

            if (seats > 9)
            {
                throw new Exception("You can not provide more than 9 seats");
            }


            Seats = seats;
        }

        public static Vehicle Create(string make, string name, int seats)
            => new Vehicle(make, name, seats);
    }
}