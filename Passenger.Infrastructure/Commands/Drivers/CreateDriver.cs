﻿namespace Passenger.Infrastructure.Commands.Drivers
{
    public class CreateDriver : AuthenticatedCommandBase
    {
        public string Name { get; set; }
        public string Make { get; set; }
    }
}