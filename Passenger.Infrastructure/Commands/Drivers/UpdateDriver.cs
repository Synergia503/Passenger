﻿namespace Passenger.Infrastructure.Commands.Drivers
{
    public class UpdateDriver : AuthenticatedCommandBase
    {
        public string Name { get; set; }
        public string Make { get; set; }
    }
}