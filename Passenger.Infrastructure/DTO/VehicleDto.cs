﻿
namespace Passenger.Infrastructure.DTO
{
    public class VehicleDto
    {
        public string Make { get; set; }
        public string Name { get; set; }
        public int Seats { get; set; }
    }
}