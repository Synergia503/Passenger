﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson;

namespace Passenger.Infrastructure.Mongo
{
    public static class MongoConfigurator
    {
        private static bool _initialized;
        public static void Initialize()
        {
            if (_initialized)
            {
                return;
            }

            _initialized = true;
            RegisterConventions();
        }

        private static void RegisterConventions()
            => ConventionRegistry.Register(
                "PassengerConventions", new MongoConventions(), x => true);

        private class MongoConventions : IConventionPack
        {
            public IEnumerable<IConvention> Conventions
                => new List<IConvention>
                {
                    new IgnoreExtraElementsConvention(true),
                    new CamelCaseElementNameConvention(),
                    new EnumRepresentationConvention(BsonType.String)
                };
        }
    }
}