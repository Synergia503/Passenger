﻿namespace Passenger.Infrastructure.Mongo
{
    class MongoSettings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}