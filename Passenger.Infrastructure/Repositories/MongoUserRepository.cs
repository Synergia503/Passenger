﻿using Passenger.Core.Repositories;
using System;
using System.Collections.Generic;
using Passenger.Core.Domain;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using MongoDB.Driver;

namespace Passenger.Infrastructure.Repositories
{
    public class MongoUserRepository : IUserRepository, IMongoRepository
    {
        private readonly IMongoDatabase _mongoDatabase;
        private IMongoCollection<User> Users => _mongoDatabase.GetCollection<User>("Users");

        public MongoUserRepository(IMongoDatabase mongoDatabase)
        {
            _mongoDatabase = mongoDatabase;
        }

        public async Task AddAsync(User user)
            => await Users.InsertOneAsync(user);

        public async Task<IEnumerable<User>> BrowseAsync()
            => await Users.AsQueryable().ToListAsync();

        public async Task<User> GetAsync(Guid id)
            => await Users.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);


        public async Task<User> GetAsync(string email)
            => await Users.AsQueryable().FirstOrDefaultAsync(x => x.Email.ToLower() == email.ToLowerInvariant());

        public async Task RemoveAsync(Guid id)
            => await Users.DeleteOneAsync(x => x.Id == id);

        public async Task UpdateAsync(User user)
            => await Users.ReplaceOneAsync(x => x.Id == user.Id, user);
    }
}