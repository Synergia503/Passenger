﻿using Passenger.Core.Repositories;
using System;
using System.Collections.Generic;
using Passenger.Core.Domain;
using System.Threading.Tasks;
using Passenger.Infrastructure.EF;
using Microsoft.EntityFrameworkCore;

namespace Passenger.Infrastructure.Repositories
{
    public class SqlUserRepository : IUserRepository, ISqlRepository
    {
        private readonly PassengerContext _passengerContext;
        public SqlUserRepository(PassengerContext passengerContext)
        {
            _passengerContext = passengerContext;
        }

        public async Task AddAsync(User user)
        {
            await _passengerContext.Users.AddAsync(user);
            await _passengerContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<User>> BrowseAsync()
            => await _passengerContext.Users.ToListAsync();

        public async Task<User> GetAsync(Guid id)
            => await _passengerContext.Users.SingleOrDefaultAsync(x => x.Id == id);
        // There would be join on table and EF would fetch Friends collection as well
        //  => await _passengerContext.Users.Include(x => x.Friends).SingleOrDefaultAsync(x => x.Id == id);

        public async Task<User> GetAsync(string email)
          => await _passengerContext.Users.SingleOrDefaultAsync(x => x.Email == email);

        public async Task RemoveAsync(Guid id)
        {
            var user = await GetAsync(id);
            _passengerContext.Users.Remove(user);
            await _passengerContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(User user)
        {
            _passengerContext.Users.Update(user);
            await _passengerContext.SaveChangesAsync();
        }
    }
}