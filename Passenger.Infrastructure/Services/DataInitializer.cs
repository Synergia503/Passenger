﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Passenger.Infrastructure.Services
{
    public class DataInitializer : IDataInitializer
    {
        private readonly IUserService _userService;
        private readonly IDriverService _driverService;
        private readonly ILogger<DataInitializer> _logger;
        private readonly IDriverRouteService _driverRouteService;

        public DataInitializer(IUserService userService, IDriverService driverService, ILogger<DataInitializer> logger, IDriverRouteService driverRouteService)
        {
            _userService = userService;
            _driverService = driverService;
            _logger = logger;
            _driverRouteService = driverRouteService;
        }

        public async Task SeedAsync()
        {
            var users = await _userService.BrowseAsync();
            if (users.Any())
            {
                return;
            }

            _logger.LogTrace("Initializing data...");
            var tasks = new List<Task>();
            for (int i = 0; i < 10; i++)
            {
                var userId = Guid.NewGuid();
                var username = $"user{i}";

                await _userService.RegisterAsync(userId, $"{username}@test.com", username, $"secret{i}", "user");
                await _driverService.CreateAsync(userId);
                await _driverService.SetVehicleAsync(userId, "BMW", "i8");
                await _driverRouteService.AddAsync(userId, "Default route", 1, 1, 2, 2);
                await _driverRouteService.AddAsync(userId, "Job route", 5, 6, 12, 22);
            }

            for (int i = 0; i < 3; i++)
            {
                var userId = Guid.NewGuid();
                var username = $"admin{i}";

                await _userService.RegisterAsync(userId, $"{username}@test.com", username, $"super_secret{i}", "admin");
            }

            await Task.WhenAll(tasks);
            _logger.LogTrace("Data was initialized");
        }
    }
}