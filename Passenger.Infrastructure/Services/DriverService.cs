﻿using System;
using System.Collections.Generic;
using Passenger.Infrastructure.DTO;
using Passenger.Core.Repositories;
using AutoMapper;
using System.Threading.Tasks;
using Passenger.Core.Domain;
using Passenger.Infrastructure.Extensions;
using Passenger.Infrastructure.Exceptions;

namespace Passenger.Infrastructure.Services
{
    public class DriverService : IDriverService
    {
        private readonly IDriverRepository _driverRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IVehicleProvider _vehicleProvider;

        public DriverService(IDriverRepository driverRepository, IMapper mapper, IUserRepository userRepository, IVehicleProvider vehicleProvider)
        {
            _driverRepository = driverRepository;
            _mapper = mapper;
            _userRepository = userRepository;
            _vehicleProvider = vehicleProvider;
        }

        public async Task<IEnumerable<DriverDto>> BrowseAsync()
        {
            var drivers = await _driverRepository.BrowseAsync();
            return _mapper.Map<IEnumerable<DriverDto>>(drivers);
        }

        public async Task CreateAsync(Guid userId)
        {
            var user = await _userRepository.GetOrFailAsync(userId);

            var driver = await _driverRepository.GetAsync(userId);
            if (driver != null)
            {
                throw new ServiceException(Exceptions.ErrorCodes.DriverAlreadyExists, $"Driver with id: '{userId}' already exists");
            }

            driver = new Driver(user);
            await _driverRepository.AddAsync(driver);
        }

        public async Task DeleteAsync(Guid userId)
        {
            var driver = await _driverRepository.GetOrFailAsync(userId);
            await _driverRepository.DeleteAsync(driver);
        }

        public async Task<DriverDetailsDto> GetAsync(Guid userId)
        {
            var driver = await _driverRepository.GetOrFailAsync(userId);

            return _mapper.Map<DriverDetailsDto>(driver);
        }

        public async Task SetVehicleAsync(Guid userId, string make, string name)
        {
            var driver = await _driverRepository.GetOrFailAsync(userId);

            var vehicleDetails = await _vehicleProvider.GetAsync(make, name);
            var vehicle = Vehicle.Create(vehicleDetails.Make, vehicleDetails.Name, vehicleDetails.Seats);
            driver.SetVehicle(vehicle);
            await _driverRepository.UpdateAsync(driver);
        }
    }
}