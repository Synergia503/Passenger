﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Passenger.Core.Domain;
using Passenger.Infrastructure.DTO;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;

namespace Passenger.Infrastructure.Services
{
    public class VehicleProvider : IVehicleProvider
    {
        private readonly IMemoryCache _cache;
        private readonly static string cacheKey = "vehicles";

        private static readonly IDictionary<string, IEnumerable<VehicleDetails>> availableVehicles =
            new Dictionary<string, IEnumerable<VehicleDetails>>
            {
                ["Audi"] = new List<VehicleDetails>
                {
                    new VehicleDetails("RS8", 5),
                    new VehicleDetails("TT", 2)
                },
                ["BMW"] = new List<VehicleDetails>
                {
                    new VehicleDetails("i8", 3),
                    new VehicleDetails("E36", 5)
                },
                ["Ford"] = new List<VehicleDetails>
                {
                    new VehicleDetails("Fiesta", 5),
                    new VehicleDetails("Focus", 7),
                    new VehicleDetails("Ka", 3)
                },
                ["Skoda"] = new List<VehicleDetails>
                {
                    new VehicleDetails("Fabia", 5),
                    new VehicleDetails("Rapid", 5)
                },
                ["Volkswagen"] = new List<VehicleDetails>
                {
                    new VehicleDetails("Passat", 5),
                }
            };

        public VehicleProvider(IMemoryCache cache)
        {
            _cache = cache;
        }

        public async Task<IEnumerable<VehicleDto>> BrowseAsync()
        {
            var vehicles = _cache.Get<IEnumerable<VehicleDto>>(cacheKey);

            if (vehicles == null)
            {
                vehicles = await GetAllAsync();
                _cache.Set(cacheKey, vehicles);
            }

            return vehicles;
        }

        public async Task<VehicleDto> GetAsync(string make, string name)
        {
            if (!availableVehicles.ContainsKey(make))
            {
                throw new Exception($"Vehicle make: '{make}' is not available.");
            }

            var vehicles = availableVehicles[make];
            var vehicle = vehicles.SingleOrDefault(x => x.Name == name);
            if (vehicle == null)
            {
                throw new Exception($"Vehicle: '{name}' for make: '{make}' is not available.");
            }

            return await Task.FromResult(new VehicleDto()
            {
                Make = make,
                Name = vehicle.Name,
                Seats = vehicle.Seats
            });
        }

        public async Task<IEnumerable<VehicleDto>> GetAllAsync()
            => await Task.FromResult(availableVehicles.GroupBy(x => x.Key)
                 .SelectMany(g => g.SelectMany(v => v.Value.Select(c => new VehicleDto
                 {
                     Make = v.Key,
                     Name = c.Name,
                     Seats = c.Seats
                 }))));


        private class VehicleDetails
        {
            public VehicleDetails(string name, int seats)
            {
                Name = name;
                Seats = seats;
            }

            public string Name { get; }
            public int Seats { get; }
        }
    }
}