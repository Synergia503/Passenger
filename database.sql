﻿create database Passenger

use Passenger

create table Users (
Id uniqueidentifier primary key not null,
Email nvarchar(100) not null,
Password nvarchar(200) not null,
Salt nvarchar(200) not null,
Username nvarchar(100) not null,
Fullname nvarchar(100) null,
Role nvarchar(10) not null,
CreatedAt datetime not null,
UpdatedAt datetime not null
)